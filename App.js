import "./node_modules/@polymer/iron-icon/iron-icon.js";
import "./node_modules/@polymer/iron-icons/iron-icons.js";
import { html, render } from "./node_modules/lit-html/lit-html.js";
import { classMap } from "./node_modules/lit-html/directives/class-map.js";

class TodoList extends HTMLElement {
  constructor() {
    super();
    this.inputText = "";
    this.todo = [];
    this.attachShadow({ mode: "open" });
    this.template = (todo, inputText) => html`
      <style>
        .strike-through {
          text-decoration: line-through;
        }
        #body {
          margin-top: 50px;
        }
        #todoText {
          width: 150px;
          padding: 12px 20px;
          margin: 8px 0;
          display: inline-block;
          border: 1px solid #ccc;
          border-radius: 4px;
          box-sizing: border-box;
        }
        #addTodo {
          background-color: #4caf50;
          border: none;
          color: white;
          padding: 12px 28px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
        }
        .todo-element {
          margin-left: 10px;
          margin-top: 10px;
        }
        .todo-text {
          margin-left: 3px;
        }
        .delete-button {
          display: none;
        }
        .active {
          display: inline;
        }
        iron-icon {
          cursor: pointer;
        }
      </style>
      <div id="body">
        <input
          type="text"
          id="todoText"
          placeholder="What's on your mind"
          @input=${this.handleChange}
          .value=${inputText}
        />
        <button id="addTodo" @click=${this.handleSubmit}>Add</button>
        <div id="todos">
          ${todo.map((todoLi, index) => {
            const textClass = {
              "strike-through": todoLi.completed,
            };
            const buttonClass = {
              active: todoLi.active,
            };
            return html`<div
              class="todo-element"
              @mouseover=${() => this.handleActive(index)}
              @mouseout=${() => this.handleInactive(index)}
            >
              <input
                type="checkbox"
                class="action-button"
                .checked=${todoLi.completed}
                @click=${() => this.markComplete(index)}
              />
              <label class="todo-text ${classMap(textClass)}">
                ${todoLi.text}
              </label>
              <span class="delete-button ${classMap(buttonClass)}">
                <iron-icon
                  icon="icons:delete"
                  @click=${() => this.handleDelete(index)}
                ></iron-icon>
              </span>
            </div>`;
          })}
        </div>
      </div>
    `;
    this.doRender();
  }
  /**
   * Renders the updated component
   */
  doRender() {
    render(this.template(this.todo, this.inputText), this.shadowRoot);
  }

  /**
   *
   * @param {Number} index
   * marks the todo task as completed
   */
  markComplete = (index) => {
    this.todo[index] = {
      ...this.todo[index],
      completed: !this.todo[index].completed,
    };
    this.doRender();
  };

  /**
   *
   * @param {Number} index
   * marks currently hovered task as active
   */
  handleActive = (index) => {
    this.todo[index] = {
      ...this.todo[index],
      active: true,
    };
    this.doRender();
  };

  /**
   *
   * @param {Number} index
   * marks task as inactive when cursor leaves
   */
  handleInactive = (index) => {
    this.todo[index] = {
      ...this.todo[index],
      active: false,
    };
    this.doRender();
  };
  /**
   *
   * @param {Number} index
   * Deletes the task
   */
  handleDelete = (index) => {
    this.todo.splice(index, 1);
    this.doRender();
  };

  handleChange = (event) => {
    this.inputText = event.target.value.trim();
    this.doRender();
  };

  /**
   * Adds input in the todolist
   */
  handleSubmit = () => {
    if (this.inputText) {
      this.todo.push({ text: this.inputText, completed: false, active: false });
      this.inputText = "";
      this.doRender();
    }
  };
}

window.customElements.define("todo-list", TodoList);
